package com.dailypractice.departmentservice.service;

import com.dailypractice.departmentservice.entity.Department;
import com.dailypractice.departmentservice.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService {

    @Autowired
    DepartmentRepository departmentRepository;

    public Department saveDepartment(Department department){
        return departmentRepository.save(department);
    }

    public Department findDepartmentById(Long departmentId) {
         return departmentRepository.findByDepartmentId(departmentId);
    }
}
