package com.dailypractice.departmentservice.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "department_Id")
    private Long departmentId;

    @Column(name = "department_Name")
    private String departmentName;
    @Column(name = "department_Address")
    private String departmentAddress;
    @Column(name = "department_Code")
    private String departmentCode;


}
